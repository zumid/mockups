rm -r build
mkdir build
inkscape --export-type=png src/_Hybrid.svg -o build/_Hybrid.png
ffmpeg -i build/_Hybrid.png -i src/Windows_updates.gif -filter_complex "[0][1]overlay=280:H-150" -c:v huffyuv -r 12 -y -an build/_Hybrid.avi
ffmpeg -i build/_Hybrid.png -vf "palettegen=max_colors=16:reserve_transparent=0" build/_Hybrid_palette.png
ffmpeg -i build/_Hybrid.avi -i build/_Hybrid_palette.png -lavfi paletteuse -r 12 build/_Hybrid.gif
