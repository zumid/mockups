rm -r build
mkdir build
python cycle_colors.py src/Hybrid_cycle_base.png src/Cyclecolors.txt build/Hybrid
ffmpeg -framerate 6 -i build/Hybrid_%01d.png -c:v huffyuv -y -an build/_Hybrid.avi
ffmpeg -i build/_Hybrid.avi -vf "palettegen=max_colors=16:reserve_transparent=0" -y build/_Hybrid_palette.png
ffmpeg -i build/_Hybrid.avi -i build/_Hybrid_palette.png -lavfi paletteuse -r 6 -y build/_Hybrid.gif
